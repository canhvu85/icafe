document.querySelector('#login .city .custom-select-wrapper').addEventListener('click', function () {
    this.querySelector('#login .city .custom-select').classList.toggle('open');
})

for (const option of document.querySelectorAll("#login .city .custom-option")) {
    option.addEventListener('click', function () {
        if (!this.classList.contains('selected')) {
            this.parentNode.querySelector('#login .city .custom-option.selected').classList.remove('selected');
            this.classList.add('selected');
            this.closest('#login .city .custom-select').querySelector('#login .city .custom-select__trigger span').textContent = this.textContent;
        }
    })
}

window.addEventListener('click', function (e) {
    const select = document.querySelector('#login .city .custom-select')
    if (!select.contains(e.target)) {
        select.classList.remove('open');
    }
});



document.querySelector('#register .city .custom-select-wrapper').addEventListener('click', function () {
    this.querySelector('#register .city .custom-select').classList.toggle('open');
})

for (const option of document.querySelectorAll("#register .city .custom-option")) {
    option.addEventListener('click', function () {
        if (!this.classList.contains('selected')) {
            this.parentNode.querySelector('#register .city .custom-option.selected').classList.remove('selected');
            this.classList.add('selected');
            this.closest('#register .city .custom-select').querySelector('#register .city .custom-select__trigger span').textContent = this.textContent;
        }
    })
}

window.addEventListener('click', function (e) {
    const select = document.querySelector('#register .city .custom-select')
    if (!select.contains(e.target)) {
        select.classList.remove('open');
    }
});





document.querySelector('#login .shop .custom-select-wrapper').addEventListener('click', function () {
    this.querySelector('#login .shop .custom-select').classList.toggle('open');
})



window.addEventListener('click', function (e) {
    const select = document.querySelector('#login .shop .custom-select')
    if (!select.contains(e.target)) {
        select.classList.remove('open');
    }
});


document.querySelector('#register .shop .custom-select-wrapper').addEventListener('click', function () {
    this.querySelector('#register .shop .custom-select').classList.toggle('open');
})



window.addEventListener('click', function (e) {
    const select = document.querySelector('#register .shop .custom-select')
    if (!select.contains(e.target)) {
        select.classList.remove('open');
    }
});
