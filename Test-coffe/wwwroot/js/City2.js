﻿var user = user || {};

user.openCreate = function () {
    $("#showCreate").modal("show");
    $("#name").val("");
    $("#avatar").val("");
    $("#blah").attr("src", "/uploads/default.png");
};

user.save = function () {
    var name = $("#name").val();
    var permalink = toSlug(name);
    // init form data:
    var formData = new FormData();
    // append data
    formData.append('name', name);
    formData.append('permalink', permalink);
    // get data
    //formData.get('username'); // Returns "Chris"
    var files = $("#avatar").get(0).files;
    // Add the uploaded image content to the form data collection
    if (files.length > 0) {
        formData.append('avatarFile', files[0]);
        formData.append('slugAvatar', toSlug(files[0].name.split(".")[0]) + "." + files[0].name.split(".")[1]);
    }
    createBtn(formData);
}

function createBtn(city) {
    $.ajax({
        url: "https://localhost:5001/api/CitiesAPI",
        method: "POST",
        contentType: false,
        processData: false,
        data: city,
        success: function (data) {
            addFirst(data.id);
        }
    });
}

$(document).ready(function () {
    showList();
    //$("#avatar").change(function () {
    //    readURL(this, "#avatar");
    //});
    $("#avatarEdit").change(function () {
        readURL(this, "#avatarPreEdit");
    });
});

function addFirst(n) {
    sendMessage("Đã tạo thành công city mới !");
    $.ajax({
        url: "https://localhost:5001/api/CitiesAPI/" + n,
        method: "GET",
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
                $("#tbList").prepend(
                    "<tr>" +
                    "<td>" + data.name + "</td>" +
                    "<td>" + data.permalink + "</td>" +
                    "<td>" + "<img style='height:100px; width:100px' src='uploads/" + defaultImage(data.avatar, data.id, "/avatar/") + "' />" + "</td>" +
                    "<td>" + "<img style='height:100px; width:100px' src='uploads/" + defaultImage(data.thumb, data.id, "/thumb/") + "' />" + "</td>" +
                    "<td><a href='javascript:;' class='btn btn-block btn-primary btn-flat'  onclick='user.openEdit(" + data.id + ",this)'>Edit</a></td>" +
                    "<td><a href='javascript:;' class='btn btn-block btn-primary btn-flat' onclick='return deleteItem(" + data.id + ",this)'>Delete</a></td>" +
                    "</tr>"
                );
        }
    });
}

function showList() {
    $.ajax({
        url: "https://localhost:5001/api/CitiesAPI",
        method: "GET",
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            $("#tbList").html("");
            $.each(data, function (index, value) {
                $("#tbList").prepend(
                    "<tr>" +
                    "<td>" + value.name + "</td>" +
                    "<td>" + value.permalink + "</td>" +
                    "<td>" + "<img style='height:100px; width:100px' src='uploads/" + defaultImage(value.avatar, value.id, "/avatar/") + "' />" + "</td>" +
                    "<td>" + "<img style='height:100px; width:100px' src='uploads/" + defaultImage(value.thumb, value.id, "/thumb/") + "' />" + "</td>" +
                    "<td><a href='javascript:;' class='btn btn-block btn-primary btn-flat'  onclick='user.openEdit(" + value.id + ",this)'>Edit</a></td>" +
                    "<td><a href='javascript:;' class='btn btn-block btn-primary btn-flat' onclick='return deleteItem(" + value.id + ",this)'>Delete</a></td>" +
                    "</tr>"
                );
            });
        }
    });
}

var idEdit;
var rowIndex;
var arr;
user.openEdit = function (id,e) {
    idEdit = id;
    rowIndex = $(e).closest("tr").index();
    getEditInfo(rowIndex);
    $("#avatarEdit").val("");
    $("#showEdit").modal("show");
};

function getEditInfo(rowIndex) {
    //$.ajax({
    //    url: "https://localhost:5001/api/CitiesAPI/" + id,
    //    method: "GET",
    //    dataType: "json",
    //    contentType: "application/json",
    //    success: function (data) {
    //        $("#nameEdit").val(data.name);
    //    }
    //});
    arr = [];
    $("#tbList tr:eq(" + rowIndex + ") td").each(function () {
        arr.push($(this).text());
    });
    $("#nameEdit").val(arr[0]);
    //$("#permalinkEdit").val(arr[1]);
    $("#avatarPreEdit").attr('src', $("#tbList tr:eq(" + rowIndex + ") td:eq(2) img").attr("src"));
}

user.edit = function () {
    var name = $("#nameEdit").val();
    var permalink = toSlug(name);
    // init form data:
    var formData = new FormData();
    // append data
    formData.append('name', name);
    formData.append('permalink', permalink);
    // get data
    //formData.get('username'); // Returns "Chris"
    var files = $("#avatarEdit").get(0).files;
    // Add the uploaded image content to the form data collection
    if (files.length > 0) {
        formData.append('avatarFile', files[0]);
        formData.append('slugAvatar', toSlug(files[0].name.split(".")[0]) + "." + files[0].name.split(".")[1]);
    }
    editBtn(idEdit,formData);
};

function editBtn(idEdit, city) {

    $.ajax({
        url: "https://localhost:5001/api/CitiesAPI/" + idEdit,
        method: "PUT",
        contentType: false,
        processData: false,
        data: city,
        success: function (data) {
            Swal.fire(
                'Thông báo',
                'Thêm thông tin thành công.',
                'success'
            );
            //sendMessage("Đã sửa thành công city mới !");
        $("#tbList tr:eq(" + rowIndex + ")").html(
            "<td>" + city.get('name') + "</td>" +
            "<td>" + city.get('permalink') + "</td>" +
            "<td>" + "<img style='height:100px; width:100px' src='uploads/" + defaultImage(data.avatar, idEdit, "/avatar/") + "' />" + "</td>" +
            "<td>" + "<img style='height:100px; width:100px' src='uploads/" + defaultImage(data.thumb, idEdit, "/thumb/") + "' />" + "</td>" +
            "<td><a href='javascript:;' class='btn btn-block btn-primary btn-flat'  onclick='user.openEdit(" + idEdit + ",this)'>Edit</a></td>" +
            "<td><a href='javascript:;' class='btn btn-block btn-primary btn-flat' onclick='return deleteItem(" + idEdit + ")'>Delete</a></td>"
                    );
        }
    });
}


function sendMessage(message) {
    $(".alert, .alert-success").show();
    $(".alert, .alert-success").html(message);
    $(".alert, .alert-success").fadeTo(2000, 500).slideUp(500, function () {
        $(".alert, .alert-success").slideUp(2000);
    });
}


function readURL(input, elem) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            console.log(elem);
            $(elem).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function deleteItem(car_id, e) {
    rowIndex = $(e).closest("tr").index();
    Swal.fire({
        title: 'Bạn chắc chắn muốn xóa thông tin này?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Vâng, xóa nó đi!',
        cancelButtonText: 'Hủy',
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "https://localhost:5001/api/CitiesAPI/" + car_id,
        method: "DELETE",
        dataType: "json",
        contentType: "application/json",
        success: function () {
            $("#tbList tr:eq(" + rowIndex + ")").remove();
        }
    });
        }
    })
}

function toSlug(str) {
    // Chuyển hết sang chữ thường
    str = str.toLowerCase();

    // xóa dấu
    str = str
        .normalize('NFD') // chuyển chuỗi sang unicode tổ hợp
        .replace(/[\u0300-\u036f]/g, ''); // xóa các ký tự dấu sau khi tách tổ hợp

    // Thay ký tự đĐ
    str = str.replace(/[đĐ]/g, 'd');

    // Xóa ký tự đặc biệt
    str = str.replace(/([^0-9a-z-\s])/g, '');

    // Xóa khoảng trắng thay bằng ký tự -
    str = str.replace(/(\s+)/g, '-');

    // Xóa ký tự - liên tiếp
    str = str.replace(/-+/g, '-');

    // xóa phần dư - ở đầu & cuối
    str = str.replace(/^-+|-+$/g, '');

    // return
    return str;
}

function defaultImage(img, id, n) {
    if (img == null) {
        return "default.png";
    } else
        return "city/" + id + n + img;
}