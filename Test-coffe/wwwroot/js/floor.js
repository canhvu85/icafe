﻿
function addItem(item) {
    if (item.trim().length > 0) {
        //arr.push(item.trim());
        let newData = {
            "name": item.trim(),
            "permalink": toSlug(item.trim()),
            'shopsId': 1
        };
        $.ajax({
            url: "/api/FloorsApi",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(newData)
        }).done(function (data) {
            Swal.fire(
                'Thông báo',
                'Tạo tầng mới thành công.',
                'success'
            );
            displayItems(1);
        }).fail(function () {
            Swal.fire({
                icon: 'error',
                title: 'Cảnh báo',
                text: 'Đã xảy ra lỗi!',
            });
        });
        // displayItems($("#FilterFloor").val());
    }
    else {
        $("#item").select();
    }
}

var listFloor = [];
displayItems(1);

function displayItems(shop_id) {
    shop_id *= 1;
    var st = "/api/FloorsApi/";
    if (shop_id != "") {
        st += "?shop_id=" + shop_id;
    }

    $.ajax({
        url: st,
        method: "GET",
        dataType: "json",
        async: false,
        contentType: "application/json"
    }).done(function (data) {
        listFloor = data;
        let text = '';
        for (let i = 0; i < data.length; i++) {
            text += '<tr>';
            text += '<th scope="row">' + (i + 1) + '</th>';
            text += '<td id="td' + data[i].id + '"><span id="span' + data[i].id + '">' + data[i].name + '</span></td>';          
            text += '<td id="tdEdit' + data[i].id + '">' + "<button id='btnEdit" + data[i].id + "' onclick='editItem(" + data[i].id + ",\"" + data[i].name + "\");' class='btn btnEdit'><i class='fa fa-edit edit-btn'></i>Sửa</button>" + '</td>';
            text += '<td>' + "<button id='btnDel" + data[i].id + "' onclick='deleteItem(" + data[i].id + ")'class='btn btnDel'><i class='fa fa-trash-alt delete-btn'></i>Xóa</button>" + '</td>';
            text += '</tr>';
        }

        //document.getElementById('countItem').innerHTML = data.length + " bàn";
        //if (data.length > 1) {
        //    document.getElementById('countItem').innerHTML += "s";
        //}
        document.getElementById("tbody").innerHTML = text;
    });
}

function editItem(tdid, val) {
    tdid *= 1;
    var input, container = document.getElementById("td" + tdid);
    input = document.createElement("input");
    input.type = "text";
    input.classList = "form-control col-sm-9 mr-3 bg-success text-white";
    input.value = val;
    container.appendChild(input);

    var tdEdit = document.getElementById("tdEdit" + tdid);
    var btnChange = document.createElement("button")
    btnChange.classList = "btn btn-primary bg-info border-0 mr-3";
    btnChange.innerHTML = 'Lưu';

    btnChange.onclick = function () {
        //arr[tdid] = input.value;
        var newData = {
            'id': tdid,
            'name': input.value.trim(),
            'permalink': toSlug(input.value.trim())          
        }
        $.ajax({
            url: "/api/FloorsApi/" + tdid,
            method: "PUT",
            dataType: "json",
            async: false,
            contentType: "application/json",
            data: JSON.stringify(newData)
        }).fail(function () {
            $("#showEdit").modal("hide");
            Swal.fire({
                icon: 'error',
                title: 'Cảnh báo',
                text: 'Đã xảy ra lỗi!',
            });
        }).done(function (result) {
            Swal.fire(
                'Thông báo',
                'Sửa thông tin thành công.',
                'success'
            );
            $(btnChange).remove();
            $(btnCancel).remove();
            $(".btnEdit").css("visibility", 'visible');
            $(".btnDel").css("visibility", 'visible');
            $(input).remove();
            $("#span" + tdid).show();
          
            displayItems(1);
        });

    };
    tdEdit.appendChild(btnChange);

    var btnCancel = document.createElement("button");
    btnCancel.classList = "btn btn-primary bg-secondary border-0";
    btnCancel.innerHTML = "Đóng";
    btnCancel.onclick = function () {
        $(btnChange).remove();
        $(btnCancel).remove();
        $(".btnEdit").css("visibility", 'visible');
        $(".btnDel").css("visibility", 'visible');
        $(input).remove();
        $("#span" + tdid).show("slow");

        displayItems(1);
    };
    tdEdit.appendChild(btnCancel);

    $("#span" + tdid).hide();   
    $(".btnEdit").css("visibility", 'hidden');
    $(".btnDel").css("visibility", 'hidden');
}

function deleteItem(id) {
    //var conf = confirm("Are you sure you want to delete \"" + arr[i] + "\" product?");
    //if (conf) {
    //    arr.splice(i, 1);
    //    displayItems();
    //}
    Swal.fire({
        title: 'Bạn chắc chắn muốn xóa tầng này?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Vâng, xóa nó đi!',
        cancelButtonText: 'Hủy',
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: '/api/FloorsApi/del/' + parseInt(id) + "/?name=vu",
                type: 'PUT',
                dataType: "json",
                contentType: "application/json"
            }).done(function() {
                    displayItems(1);
                }).fail(function (data) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Cảnh báo',
                            text: data.responseText
                        });
                 });
        }
    });
}
