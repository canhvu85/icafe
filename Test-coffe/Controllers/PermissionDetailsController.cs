﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Test_coffe.Models;

namespace Test_coffe.Controllers
{
    public class PermissionDetailsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PermissionDetailsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PermissionDetails
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.PermissionDetails.Include(p => p.Permissions).Include(p => p.Users);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: PermissionDetails/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var permissionDetail = await _context.PermissionDetails
                .Include(p => p.Permissions)
                .Include(p => p.Users)
                .FirstOrDefaultAsync(m => m.id == id);
            if (permissionDetail == null)
            {
                return NotFound();
            }

            return View(permissionDetail);
        }

        // GET: PermissionDetails/Create
        public IActionResult Create()
        {
            ViewData["PermissionId"] = new SelectList(_context.Permissions, "id", "id");
            ViewData["UserId"] = new SelectList(_context.Users, "id", "id");
            return View();
        }

        // POST: PermissionDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,PermissionId,UserId,isDeleted,deleted_at,deleted_by,created_at,created_by,updated_at,updated_by")] PermissionDetails permissionDetail)
        {
            if (ModelState.IsValid)
            {
                _context.Add(permissionDetail);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PermissionId"] = new SelectList(_context.Permissions, "id", "id", permissionDetail.PermissionsId);
            ViewData["UserId"] = new SelectList(_context.Users, "id", "id", permissionDetail.UsersId);
            return View(permissionDetail);
        }

        // GET: PermissionDetails/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var permissionDetail = await _context.PermissionDetails.FindAsync(id);
            if (permissionDetail == null)
            {
                return NotFound();
            }
            ViewData["PermissionId"] = new SelectList(_context.Permissions, "id", "id", permissionDetail.PermissionsId);
            ViewData["UserId"] = new SelectList(_context.Users, "id", "id", permissionDetail.UsersId);
            return View(permissionDetail);
        }

        // POST: PermissionDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,PermissionId,UserId,isDeleted,deleted_at,deleted_by,created_at,created_by,updated_at,updated_by")] PermissionDetails permissionDetail)
        {
            if (id != permissionDetail.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(permissionDetail);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PermissionDetailExists(permissionDetail.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PermissionId"] = new SelectList(_context.Permissions, "id", "id", permissionDetail.PermissionsId);
            ViewData["UserId"] = new SelectList(_context.Users, "id", "id", permissionDetail.UsersId);
            return View(permissionDetail);
        }

        // GET: PermissionDetails/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var permissionDetail = await _context.PermissionDetails
                .Include(p => p.Permissions)
                .Include(p => p.Users)
                .FirstOrDefaultAsync(m => m.id == id);
            if (permissionDetail == null)
            {
                return NotFound();
            }

            return View(permissionDetail);
        }

        // POST: PermissionDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var permissionDetail = await _context.PermissionDetails.FindAsync(id);
            _context.PermissionDetails.Remove(permissionDetail);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PermissionDetailExists(int id)
        {
            return _context.PermissionDetails.Any(e => e.id == id);
        }
    }
}
