﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test_coffe.Migrations
{
    public partial class up31 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_Users_Usersid",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_BillDetails_Bills_Billsid",
                table: "BillDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_BillDetails_Products_Productsid",
                table: "BillDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Bills_Tables_Tablesid",
                table: "Bills");

            migrationBuilder.DropForeignKey(
                name: "FK_Cataloges_Shops_Shopsid",
                table: "Cataloges");

            migrationBuilder.DropForeignKey(
                name: "FK_PermissionDetails_Permissions_Permissionsid",
                table: "PermissionDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_PermissionDetails_Users_Usersid",
                table: "PermissionDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Permissions_Groups_Groupsid",
                table: "Permissions");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Cataloges_Catalogesid",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Positions_Positionsid",
                table: "Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Shops_Shopsid",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "PositionId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ShopId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "CatalogeId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Permissions");

            migrationBuilder.DropColumn(
                name: "PermissionId",
                table: "PermissionDetails");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "PermissionDetails");

            migrationBuilder.DropColumn(
                name: "ShopId",
                table: "Cataloges");

            migrationBuilder.DropColumn(
                name: "TableId",
                table: "Bills");

            migrationBuilder.DropColumn(
                name: "BillId",
                table: "BillDetails");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "BillDetails");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Accounts");

            migrationBuilder.RenameColumn(
                name: "Shopsid",
                table: "Users",
                newName: "ShopsId");

            migrationBuilder.RenameColumn(
                name: "Positionsid",
                table: "Users",
                newName: "PositionsId");

            migrationBuilder.RenameIndex(
                name: "IX_Users_Shopsid",
                table: "Users",
                newName: "IX_Users_ShopsId");

            migrationBuilder.RenameIndex(
                name: "IX_Users_Positionsid",
                table: "Users",
                newName: "IX_Users_PositionsId");

            migrationBuilder.RenameColumn(
                name: "Catalogesid",
                table: "Products",
                newName: "CatalogesId");

            migrationBuilder.RenameIndex(
                name: "IX_Products_Catalogesid",
                table: "Products",
                newName: "IX_Products_CatalogesId");

            migrationBuilder.RenameColumn(
                name: "Groupsid",
                table: "Permissions",
                newName: "GroupsId");

            migrationBuilder.RenameIndex(
                name: "IX_Permissions_Groupsid",
                table: "Permissions",
                newName: "IX_Permissions_GroupsId");

            migrationBuilder.RenameColumn(
                name: "Usersid",
                table: "PermissionDetails",
                newName: "UsersId");

            migrationBuilder.RenameColumn(
                name: "Permissionsid",
                table: "PermissionDetails",
                newName: "PermissionsId");

            migrationBuilder.RenameIndex(
                name: "IX_PermissionDetails_Usersid",
                table: "PermissionDetails",
                newName: "IX_PermissionDetails_UsersId");

            migrationBuilder.RenameIndex(
                name: "IX_PermissionDetails_Permissionsid",
                table: "PermissionDetails",
                newName: "IX_PermissionDetails_PermissionsId");

            migrationBuilder.RenameColumn(
                name: "Shopsid",
                table: "Cataloges",
                newName: "ShopsId");

            migrationBuilder.RenameIndex(
                name: "IX_Cataloges_Shopsid",
                table: "Cataloges",
                newName: "IX_Cataloges_ShopsId");

            migrationBuilder.RenameColumn(
                name: "Tablesid",
                table: "Bills",
                newName: "TablesId");

            migrationBuilder.RenameIndex(
                name: "IX_Bills_Tablesid",
                table: "Bills",
                newName: "IX_Bills_TablesId");

            migrationBuilder.RenameColumn(
                name: "Productsid",
                table: "BillDetails",
                newName: "ProductsId");

            migrationBuilder.RenameColumn(
                name: "Billsid",
                table: "BillDetails",
                newName: "BillsId");

            migrationBuilder.RenameIndex(
                name: "IX_BillDetails_Productsid",
                table: "BillDetails",
                newName: "IX_BillDetails_ProductsId");

            migrationBuilder.RenameIndex(
                name: "IX_BillDetails_Billsid",
                table: "BillDetails",
                newName: "IX_BillDetails_BillsId");

            migrationBuilder.RenameColumn(
                name: "Usersid",
                table: "Accounts",
                newName: "UsersId");

            migrationBuilder.RenameIndex(
                name: "IX_Accounts_Usersid",
                table: "Accounts",
                newName: "IX_Accounts_UsersId");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_Users_UsersId",
                table: "Accounts",
                column: "UsersId",
                principalTable: "Users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BillDetails_Bills_BillsId",
                table: "BillDetails",
                column: "BillsId",
                principalTable: "Bills",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BillDetails_Products_ProductsId",
                table: "BillDetails",
                column: "ProductsId",
                principalTable: "Products",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Bills_Tables_TablesId",
                table: "Bills",
                column: "TablesId",
                principalTable: "Tables",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cataloges_Shops_ShopsId",
                table: "Cataloges",
                column: "ShopsId",
                principalTable: "Shops",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PermissionDetails_Permissions_PermissionsId",
                table: "PermissionDetails",
                column: "PermissionsId",
                principalTable: "Permissions",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PermissionDetails_Users_UsersId",
                table: "PermissionDetails",
                column: "UsersId",
                principalTable: "Users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Permissions_Groups_GroupsId",
                table: "Permissions",
                column: "GroupsId",
                principalTable: "Groups",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Cataloges_CatalogesId",
                table: "Products",
                column: "CatalogesId",
                principalTable: "Cataloges",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Positions_PositionsId",
                table: "Users",
                column: "PositionsId",
                principalTable: "Positions",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Shops_ShopsId",
                table: "Users",
                column: "ShopsId",
                principalTable: "Shops",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_Users_UsersId",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_BillDetails_Bills_BillsId",
                table: "BillDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_BillDetails_Products_ProductsId",
                table: "BillDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Bills_Tables_TablesId",
                table: "Bills");

            migrationBuilder.DropForeignKey(
                name: "FK_Cataloges_Shops_ShopsId",
                table: "Cataloges");

            migrationBuilder.DropForeignKey(
                name: "FK_PermissionDetails_Permissions_PermissionsId",
                table: "PermissionDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_PermissionDetails_Users_UsersId",
                table: "PermissionDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Permissions_Groups_GroupsId",
                table: "Permissions");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Cataloges_CatalogesId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Positions_PositionsId",
                table: "Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Shops_ShopsId",
                table: "Users");

            migrationBuilder.RenameColumn(
                name: "ShopsId",
                table: "Users",
                newName: "Shopsid");

            migrationBuilder.RenameColumn(
                name: "PositionsId",
                table: "Users",
                newName: "Positionsid");

            migrationBuilder.RenameIndex(
                name: "IX_Users_ShopsId",
                table: "Users",
                newName: "IX_Users_Shopsid");

            migrationBuilder.RenameIndex(
                name: "IX_Users_PositionsId",
                table: "Users",
                newName: "IX_Users_Positionsid");

            migrationBuilder.RenameColumn(
                name: "CatalogesId",
                table: "Products",
                newName: "Catalogesid");

            migrationBuilder.RenameIndex(
                name: "IX_Products_CatalogesId",
                table: "Products",
                newName: "IX_Products_Catalogesid");

            migrationBuilder.RenameColumn(
                name: "GroupsId",
                table: "Permissions",
                newName: "Groupsid");

            migrationBuilder.RenameIndex(
                name: "IX_Permissions_GroupsId",
                table: "Permissions",
                newName: "IX_Permissions_Groupsid");

            migrationBuilder.RenameColumn(
                name: "UsersId",
                table: "PermissionDetails",
                newName: "Usersid");

            migrationBuilder.RenameColumn(
                name: "PermissionsId",
                table: "PermissionDetails",
                newName: "Permissionsid");

            migrationBuilder.RenameIndex(
                name: "IX_PermissionDetails_UsersId",
                table: "PermissionDetails",
                newName: "IX_PermissionDetails_Usersid");

            migrationBuilder.RenameIndex(
                name: "IX_PermissionDetails_PermissionsId",
                table: "PermissionDetails",
                newName: "IX_PermissionDetails_Permissionsid");

            migrationBuilder.RenameColumn(
                name: "ShopsId",
                table: "Cataloges",
                newName: "Shopsid");

            migrationBuilder.RenameIndex(
                name: "IX_Cataloges_ShopsId",
                table: "Cataloges",
                newName: "IX_Cataloges_Shopsid");

            migrationBuilder.RenameColumn(
                name: "TablesId",
                table: "Bills",
                newName: "Tablesid");

            migrationBuilder.RenameIndex(
                name: "IX_Bills_TablesId",
                table: "Bills",
                newName: "IX_Bills_Tablesid");

            migrationBuilder.RenameColumn(
                name: "ProductsId",
                table: "BillDetails",
                newName: "Productsid");

            migrationBuilder.RenameColumn(
                name: "BillsId",
                table: "BillDetails",
                newName: "Billsid");

            migrationBuilder.RenameIndex(
                name: "IX_BillDetails_ProductsId",
                table: "BillDetails",
                newName: "IX_BillDetails_Productsid");

            migrationBuilder.RenameIndex(
                name: "IX_BillDetails_BillsId",
                table: "BillDetails",
                newName: "IX_BillDetails_Billsid");

            migrationBuilder.RenameColumn(
                name: "UsersId",
                table: "Accounts",
                newName: "Usersid");

            migrationBuilder.RenameIndex(
                name: "IX_Accounts_UsersId",
                table: "Accounts",
                newName: "IX_Accounts_Usersid");

            migrationBuilder.AddColumn<int>(
                name: "PositionId",
                table: "Users",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ShopId",
                table: "Users",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CatalogeId",
                table: "Products",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GroupId",
                table: "Permissions",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PermissionId",
                table: "PermissionDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "PermissionDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ShopId",
                table: "Cataloges",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TableId",
                table: "Bills",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BillId",
                table: "BillDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProductId",
                table: "BillDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Accounts",
                type: "int",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_Users_Usersid",
                table: "Accounts",
                column: "Usersid",
                principalTable: "Users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BillDetails_Bills_Billsid",
                table: "BillDetails",
                column: "Billsid",
                principalTable: "Bills",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BillDetails_Products_Productsid",
                table: "BillDetails",
                column: "Productsid",
                principalTable: "Products",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Bills_Tables_Tablesid",
                table: "Bills",
                column: "Tablesid",
                principalTable: "Tables",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cataloges_Shops_Shopsid",
                table: "Cataloges",
                column: "Shopsid",
                principalTable: "Shops",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PermissionDetails_Permissions_Permissionsid",
                table: "PermissionDetails",
                column: "Permissionsid",
                principalTable: "Permissions",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PermissionDetails_Users_Usersid",
                table: "PermissionDetails",
                column: "Usersid",
                principalTable: "Users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Permissions_Groups_Groupsid",
                table: "Permissions",
                column: "Groupsid",
                principalTable: "Groups",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Cataloges_Catalogesid",
                table: "Products",
                column: "Catalogesid",
                principalTable: "Cataloges",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Positions_Positionsid",
                table: "Users",
                column: "Positionsid",
                principalTable: "Positions",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Shops_Shopsid",
                table: "Users",
                column: "Shopsid",
                principalTable: "Shops",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
