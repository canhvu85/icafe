﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test_coffe.Migrations
{
    public partial class up129 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "avatar",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "thumb",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "avatar",
                table: "Shops");

            migrationBuilder.DropColumn(
                name: "thumb",
                table: "Shops");

            migrationBuilder.DropColumn(
                name: "avatar",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "thumb",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "avatar",
                table: "Cities");

            migrationBuilder.DropColumn(
                name: "thumb",
                table: "Cities");

            migrationBuilder.DropColumn(
                name: "avatar",
                table: "Cataloges");

            migrationBuilder.DropColumn(
                name: "thumb",
                table: "Cataloges");

            migrationBuilder.AddColumn<string>(
                name: "images",
                table: "Users",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "images",
                table: "Shops",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "images",
                table: "Products",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "name",
                table: "Cities",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "images",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "images",
                table: "Shops");

            migrationBuilder.DropColumn(
                name: "images",
                table: "Products");

            migrationBuilder.AddColumn<string>(
                name: "avatar",
                table: "Users",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "thumb",
                table: "Users",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "avatar",
                table: "Shops",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "thumb",
                table: "Shops",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "avatar",
                table: "Products",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "thumb",
                table: "Products",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "name",
                table: "Cities",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 255);

            migrationBuilder.AddColumn<string>(
                name: "avatar",
                table: "Cities",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "thumb",
                table: "Cities",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "avatar",
                table: "Cataloges",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "thumb",
                table: "Cataloges",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);
        }
    }
}
