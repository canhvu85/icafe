﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test_coffe.Migrations
{
    public partial class up123 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "total_money",
                table: "Bills",
                type: "decimal(8,0)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,0)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "sub_total",
                table: "Bills",
                type: "decimal(8,0)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,0)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "fee_service",
                table: "Bills",
                type: "decimal(8,0)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,0)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "total_money",
                table: "Bills",
                type: "decimal(8,0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,0)");

            migrationBuilder.AlterColumn<decimal>(
                name: "sub_total",
                table: "Bills",
                type: "decimal(8,0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,0)");

            migrationBuilder.AlterColumn<decimal>(
                name: "fee_service",
                table: "Bills",
                type: "decimal(8,0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,0)");
        }
    }
}
