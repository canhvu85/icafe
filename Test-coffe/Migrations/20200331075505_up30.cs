﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test_coffe.Migrations
{
    public partial class up30 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Floors_Shops_Shopsid",
                table: "Floors");

            migrationBuilder.DropForeignKey(
                name: "FK_Shops_Cities_Citiesid",
                table: "Shops");

            migrationBuilder.DropForeignKey(
                name: "FK_Tables_Floors_Floorsid",
                table: "Tables");

            migrationBuilder.DropColumn(
                name: "FloorId",
                table: "Tables");

            migrationBuilder.DropColumn(
                name: "CityId",
                table: "Shops");

            migrationBuilder.DropColumn(
                name: "ShopId",
                table: "Floors");

            migrationBuilder.RenameColumn(
                name: "Floorsid",
                table: "Tables",
                newName: "FloorsId");

            migrationBuilder.RenameIndex(
                name: "IX_Tables_Floorsid",
                table: "Tables",
                newName: "IX_Tables_FloorsId");

            migrationBuilder.RenameColumn(
                name: "Citiesid",
                table: "Shops",
                newName: "CitiesId");

            migrationBuilder.RenameIndex(
                name: "IX_Shops_Citiesid",
                table: "Shops",
                newName: "IX_Shops_CitiesId");

            migrationBuilder.RenameColumn(
                name: "Shopsid",
                table: "Floors",
                newName: "ShopsId");

            migrationBuilder.RenameIndex(
                name: "IX_Floors_Shopsid",
                table: "Floors",
                newName: "IX_Floors_ShopsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Floors_Shops_ShopsId",
                table: "Floors",
                column: "ShopsId",
                principalTable: "Shops",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Shops_Cities_CitiesId",
                table: "Shops",
                column: "CitiesId",
                principalTable: "Cities",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tables_Floors_FloorsId",
                table: "Tables",
                column: "FloorsId",
                principalTable: "Floors",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Floors_Shops_ShopsId",
                table: "Floors");

            migrationBuilder.DropForeignKey(
                name: "FK_Shops_Cities_CitiesId",
                table: "Shops");

            migrationBuilder.DropForeignKey(
                name: "FK_Tables_Floors_FloorsId",
                table: "Tables");

            migrationBuilder.RenameColumn(
                name: "FloorsId",
                table: "Tables",
                newName: "Floorsid");

            migrationBuilder.RenameIndex(
                name: "IX_Tables_FloorsId",
                table: "Tables",
                newName: "IX_Tables_Floorsid");

            migrationBuilder.RenameColumn(
                name: "CitiesId",
                table: "Shops",
                newName: "Citiesid");

            migrationBuilder.RenameIndex(
                name: "IX_Shops_CitiesId",
                table: "Shops",
                newName: "IX_Shops_Citiesid");

            migrationBuilder.RenameColumn(
                name: "ShopsId",
                table: "Floors",
                newName: "Shopsid");

            migrationBuilder.RenameIndex(
                name: "IX_Floors_ShopsId",
                table: "Floors",
                newName: "IX_Floors_Shopsid");

            migrationBuilder.AddColumn<int>(
                name: "FloorId",
                table: "Tables",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CityId",
                table: "Shops",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ShopId",
                table: "Floors",
                type: "int",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Floors_Shops_Shopsid",
                table: "Floors",
                column: "Shopsid",
                principalTable: "Shops",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Shops_Cities_Citiesid",
                table: "Shops",
                column: "Citiesid",
                principalTable: "Cities",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tables_Floors_Floorsid",
                table: "Tables",
                column: "Floorsid",
                principalTable: "Floors",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
