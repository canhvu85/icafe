﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Test_coffe.Models
{
    public class Account
    {
        public int id { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal amount { get; set; }
        public string type_card { get; set; }
        public string info_card { get; set; }
        public string permalink { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }

        public bool isDeleted { get; set; } = false;
        public DateTime? deleted_at { get; set; }
        public string? deleted_by { get; set; }
        public DateTime created_at { get; set; } = DateTime.Now;
        public string? created_by { get; set; }
        public DateTime? updated_at { get; set; }
        public string? updated_by { get; set; }
    }
}
