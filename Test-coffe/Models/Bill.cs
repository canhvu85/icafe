﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Test_coffe.Models
{
    public class Bill
    {
        public int id { get; set; }
        public DateTime time_enter { get; set; }
        public DateTime time_out { get; set; }
        public string permalink { get; set; }
        public int status { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal sub_total { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal fee_service { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal total_money { get; set; }
        public string user_name { get; set; }

        public int? TableId { get; set; }
        public Table Table { get; set; }        
        [JsonIgnore]
        public ICollection<BillDetail> BillDetail { get; set; }

        public bool isDeleted { get; set; } = false;
        public DateTime? deleted_at { get; set; }
        public string? deleted_by { get; set; }
        public DateTime created_at { get; set; } = DateTime.Now;
        public string? created_by { get; set; }
        public DateTime? updated_at { get; set; }
        public string? updated_by { get; set; }
    }
}
