﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Test_coffe.Models
{
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public string avatar { get; set; }
        public string thumb { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string permalink { get; set; }

        public int? PositionId { get; set; }
        public Position Position { get; set; }
        public int? ShopId { get; set; }
        public Shop Shop { get; set; }
        [JsonIgnore]
        public ICollection<PermissionDetail> PermissionDetail { get; set; }
       
        [JsonIgnore]
        public ICollection<Account> Account { get; set; }

        public bool isDeleted { get; set; } = false;
        public DateTime? deleted_at { get; set; }
        public string? deleted_by { get; set; }
        public DateTime created_at { get; set; } = DateTime.Now;
        public string? created_by { get; set; }
        public DateTime? updated_at { get; set; }
        public string? updated_by { get; set; }
    }
}
