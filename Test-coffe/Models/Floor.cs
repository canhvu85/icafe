﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Test_coffe.Models
{
    public class Floor
    {
        public int id { get; set; }
        public string name { get; set; }
        public string permalink { get; set; }

        public int? ShopId { get; set; }
        public Shop Shop { get; set; }
        [JsonIgnore]
        public ICollection<Table> Table { get; set; }

        public bool isDeleted { get; set; } = false;
        public DateTime? deleted_at { get; set; }
        public string? deleted_by { get; set; }
        public DateTime created_at { get; set; } = DateTime.Now;
        public string? created_by { get; set; }
        public DateTime? updated_at { get; set; }
        public string? updated_by { get; set; }
    }
}
