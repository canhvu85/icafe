#pragma checksum "C:\Users\Phan Anh Vu\Desktop\cafe\vu\vu\Test-coffe\Test-coffe\Views\Mobile\Cart.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b249e2da79ae44a9892c985136d1b1455119c542"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Mobile_Cart), @"mvc.1.0.view", @"/Views/Mobile/Cart.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Phan Anh Vu\Desktop\cafe\vu\vu\Test-coffe\Test-coffe\Views\_ViewImports.cshtml"
using Test_coffe;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Phan Anh Vu\Desktop\cafe\vu\vu\Test-coffe\Test-coffe\Views\_ViewImports.cshtml"
using Test_coffe.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b249e2da79ae44a9892c985136d1b1455119c542", @"/Views/Mobile/Cart.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"04158a09f2ba594abb4a998dcb66526163e31db0", @"/Views/_ViewImports.cshtml")]
    public class Views_Mobile_Cart : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/signalr/dist/browser/signalr.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("text/javascript"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/script.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/mobile/js/tables.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\Phan Anh Vu\Desktop\cafe\vu\vu\Test-coffe\Test-coffe\Views\Mobile\Cart.cshtml"
  
    Layout = null;

#line default
#line hidden
#nullable disable
            WriteLiteral("<!DOCTYPE html>\r\n<html lang=\"en\">\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b249e2da79ae44a9892c985136d1b1455119c5425063", async() => {
                WriteLiteral(@"
    <meta charset=""UTF-8"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no"">
    <title>iCafe</title>
    <link rel=""stylesheet"" type=""text/css"" href=""css/bootstrap-4.4.1.min.css"">
    <link rel=""stylesheet"" type=""text/css"" href=""../css/font-awesome/css/font-awesome.min.css"">
    <link rel=""stylesheet"" type=""text/css"" href=""css/custom.style.css"">
    <link rel=""stylesheet"" type=""text/css"" href=""css/account.css"">

");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b249e2da79ae44a9892c985136d1b1455119c5426547", async() => {
                WriteLiteral("\r\n    <div id=\"main\">\r\n        <div id=\"nav-top\">\r\n            <nav class=\"nav_tablet_mobile hide-on-pc\">\r\n                <div");
                BeginWriteAttribute("class", " class=\"", 699, "\"", 707, 0);
                EndWriteAttribute();
                WriteLiteral(" style=\"float: left; padding: 10px;\">\r\n                    <label>Giỏ hàng</label>\r\n                </div>\r\n                <div");
                BeginWriteAttribute("class", " class=\"", 836, "\"", 844, 0);
                EndWriteAttribute();
                WriteLiteral(@" style=""float: right; padding-right: 10px; padding-top: 10px;"">
                    <a href=""javascript:;"" class=""nav_mobile-link nav-cart""><i class=""fa fa-trash-o""></i></a>
                </div>
            </nav>
        </div>
        <div class=""container"">
            <div class=""main-order-left"">
                <div class=""group-list-items-mobile"">
                    <div id=""group-item-billsdetail"" class=""list-items active"">

                    </div>
                    <div id=""group-item"" class=""list-items active"">
");
                WriteLiteral(@"                    </div>
                </div>
            </div>

            <div class=""main-order-right"">
                <div id=""main-order-1"" class=""main-order-right main-order"" style=""display: block;"">

                    <div class=""menu-order"">
                        <div id=""table-order-name"" class=""table-name"">

                        </div>
                        <div id=""table-bill-1"" class=""table-bill"">

                        </div>
                        <div id=""sub-total-money-1"" class=""sub-total-money"">
                            <div class=""col-md-8"">
                                <p><b>Tiền sản phẩm</b></p>
                                <p>Phí dịch vụ</p>
                            </div>
                            <div class=""col-md-4"" style=""text-align: right;"">
                            </div>
                        </div>
                        <div id=""total-money-1"" class=""total-money"">
                            <div class=""col-md-8"">
 ");
                WriteLiteral(@"                               <p><b>Tổng tiền</b></p>
                            </div>
                            <div class=""col-md-4"" style=""text-align: right;"">

                            </div>
                        </div>
                        <!-- <div class=""checkout"">
                            <button type=""button"" class=""btn btn-success"">Đặt món cho khách bàn 01</button>
                        </div> -->
                    </div>
                </div>

            </div>

        </div>
    </div>

    <label class=""cart_nav_overlay_mobile""></label>

    <div id=""edit-item"">
        <div class=""col-md-12"" style=""display: flex; border-bottom: solid 1px #ccc;"">
            <div class=""col-md-2"">
                <button class=""btn-close""");
                BeginWriteAttribute("style", " style=\"", 9032, "\"", 9040, 0);
                EndWriteAttribute();
                WriteLiteral(@" data-id=""1"" data-name=""Cà phê đen"" data-price=""12000""><i class=""fa fa-times""></i></button>
            </div>
            <div class=""col-md-8 item-name-selected center"">
                <span></span>
            </div>
            <div class=""col-md-2"">
                <button class=""btn-check""");
                BeginWriteAttribute("style", " style=\"", 9344, "\"", 9352, 0);
                EndWriteAttribute();
                WriteLiteral(@" data-id=""1"" data-name=""Cà phê đen"" data-price=""12000""><i class=""fa fa-check""></i></button>
            </div>
        </div>
        <div class=""col-md-12"" style=""display: block; text-align: center; line-height: 35px;"">
            <span>Số lượng</span>
        </div>
        <div class=""col-md-12"" style=""display: flex;"">
            <div class=""col-md-2"">
                <button class=""btn-minus""");
                BeginWriteAttribute("style", " style=\"", 9762, "\"", 9770, 0);
                EndWriteAttribute();
                WriteLiteral(@" data-id=""1"" data-name=""Cà phê đen"" data-price=""12000""><i class=""fa fa-minus""></i></button>
            </div>
            <div class=""col-md-8 item-count-selected center"">
                <input type=""text"" readonly>
            </div>
            <div class=""col-md-2"">
                <button class=""btn-plus""");
                BeginWriteAttribute("style", " style=\"", 10089, "\"", 10097, 0);
                EndWriteAttribute();
                WriteLiteral(" data-id=\"1\" data-name=\"Cà phê đen\" data-price=\"12000\"><i class=\"fa fa-plus\"></i></button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n\r\n    <div class=\"col-md-12 cart-checkout-fix\">\r\n        <a");
                BeginWriteAttribute("href", " href=\"", 10299, "\"", 10306, 0);
                EndWriteAttribute();
                WriteLiteral(@" class=""cart-checkout"">
            <div class=""col-md-2 cart-count-total"" style=""text-align: center;"">6</div>
            <div class=""col-md-8 cart-money-total"" style=""text-align: center;"">Gọi món </div>
            <div class=""col-md-2"" style=""text-align: right;"">
                <img src=""images/arrow-right.png"" height=""16px"">
            </div>
        </a>
    </div>

    <div class=""nav-bottom"">
        <nav class=""nav_tablet_mobile"">
            <ul class=""nav_mobile-list"">
                <li>
                    <div");
                BeginWriteAttribute("class", " class=\"", 10851, "\"", 10859, 0);
                EndWriteAttribute();
                WriteLiteral(@">
                        <a href=""index"" class=""nav_mobile-link"">
                            <i class=""fa fa-home""></i>
                            <p style=""margin-top: -10px;""><span>Trang chủ</span></p>
                        </a>
                    </div>
                </li>
                <li>
                    <div");
                BeginWriteAttribute("class", " class=\"", 11198, "\"", 11206, 0);
                EndWriteAttribute();
                WriteLiteral(">\r\n                        <a");
                BeginWriteAttribute("href", " href=\"", 11236, "\"", 11243, 0);
                EndWriteAttribute();
                WriteLiteral(@" class=""nav_mobile-link"">
                            <i class=""fa fa-table""></i>
                            <p style=""margin-top: -10px;""><span>Bàn</span></p>
                        </a>
                    </div>
                </li>

                <li>
                    <div");
                BeginWriteAttribute("class", " class=\"", 11537, "\"", 11545, 0);
                EndWriteAttribute();
                WriteLiteral(">\r\n                        <a");
                BeginWriteAttribute("href", " href=\"", 11575, "\"", 11582, 0);
                EndWriteAttribute();
                WriteLiteral(@" class=""nav_mobile-link"">
                            <i class=""fa fa-inbox""></i>
                            <p style=""margin-top: -10px;""><span>Tin nhắn</span></p>
                        </a>
                    </div>
                </li>
                <li>
                    <a");
                BeginWriteAttribute("href", " href=\"", 11877, "\"", 11884, 0);
                EndWriteAttribute();
                WriteLiteral(@" class=""nav_mobile-link"">
                        <i class=""fa fa-bell"" style=""position: absolute; top: 6px; left: 39px;""></i>
                        <span class=""label label-danger"" style=""top: -10px; position: relative; left: 20px;"">99+</span>
                        <p style=""margin-top: -10px;""><span>Thông báo</span></p>
                    </a>
                </li>
                <li>
                    <div");
                BeginWriteAttribute("class", " class=\"", 12312, "\"", 12320, 0);
                EndWriteAttribute();
                WriteLiteral(@">
                        <a href=""account.html"" class=""nav_mobile-link"">
                            <i class=""fa fa-user""></i>
                            <p style=""margin-top: -10px;""><span>Tài khoản</span></p>
                        </a>
                    </div>
                </li>
            </ul>
        </nav>
    </div>

    <script type=""text/javascript"" src=""js/jquery-3.4.1.js""></script>
    <script type=""text/javascript"" src=""js/jquery-ui-1.12.1.js""></script>
    <script type=""text/javascript"" src=""js/jquery.ui.touch-punch.min.js""></script>
    <script type=""text/javascript"" src=""js/bootstrap-4.4.1.min.js""></script>
    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b249e2da79ae44a9892c985136d1b1455119c54215764", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b249e2da79ae44a9892c985136d1b1455119c54216864", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    <!-- <script type=\"text/javascript\" src=\"js/json.js\"></script> -->\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b249e2da79ae44a9892c985136d1b1455119c54218129", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    <script type=\"text/javascript\" src=\"js/custom.script.js\"></script>\r\n    <script type=\"text/javascript\" src=\"js/page.cart.js\"></script>\r\n\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</html>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
